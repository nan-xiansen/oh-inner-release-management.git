

修订记录

 

| 日期 | 修订版本 | 修改章节 | 修改描述 |
| ---- | -------- | -------- | -------- |
|      |          |          |          |

缩略语清单： 

| 缩略语 | 英文全名 | 中文解释 |
| ------ | -------- | -------- |
|        |          |          |
|        |          |          |
|        |          |          |
|        |          |          |

# 

# 1   概述

描述本次被测对象变更内容。

# 2   测试版本说明

描述测试版本信息。

| 版本名称 | 测试起始时间 | 测试结束时间 |
| -------- | ------------ | ------------ |
|          |              |              |

描述本次测试的测试环境（包括环境软硬件版本信息，环境组网配置信息, 测试辅助工具等）。

| 硬件型号 | 硬件配置信息 | 备注 |
| -------- | ------------ | ---- |
|          |              |      |
|          |              |      |
|          |              |      |

 

# 3   版本概要测试结论

*本章节概要给出版本测试结论*

# 4   版本详细测试结论

*本章节针对总体测试策略计划的测试内容，给出详细的测试结论。*

## 4.1   特性测试结论



### 4.1.1   继承特性评价

*继承特性进行评价，用表格形式评价，包括特性列表，验证质量评估。*

XXX子系统：特性质量良好

| 序号 | 特性名称    | 特性质量评估                             | 备注 |
| ---- | ----------- | ---------------------------------------- | ---- |
| *1*  | *启动恢复子系统* | *没有新增特性，测试用例执行通过，特性质量良好* |      |
| 2 | 内核子系统 | 轻量系统无合适的公共开发板，该系统内核特性无法验证，风险高<br />小型系统稳定性压力测试存在文件删除，内存泄漏等问题，trace功能缺少指导书，toybox支持的shell命令缺少使用手册，开发者使用难度较大，风险高；<br />标准系统基本功能可用，特性质量良好<br /> | |
| *3*  | 媒体子系统 | 标准系统历史特性（音视频播放、音频管理、相机拍照）验证通过，特性质量良好<br />轻量小型系统无新增特性，基本功能存在问题（无法查看照片和视频，目前m4a,mp3,mp4文件无法播放） |      |
| 4 | 驱动子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好；<br />标准系统基本功能可用，camera/wifi驱动遗留少量问题; | |
| 5 | 泛sensor服务子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好； | |
|6|用户程序框架子系统|小型系统没有新增特性，测试用例执行通过，特性质量良好||
|7|账号子系统|特性基本可用||
|8|杂散子系统|特性基本可用||
|9|元能力子系统|特性基本可用||
|10|事件通知子系统|标准系统没有新增特性，测试用例执行通过，特性质量良好||
|11|窗口子系统|特性基本可用||
|12|电话子系统|特性基本可用||
|12|分布式文件子系统|标准系统继承特性因测试框架变更，相关JS接口用例适配未完成，出现用例BLOCK情况，影响测试，风险高||
|13|DFX子系统|基本功能可用|遗留问题：1、运行hilog的压测，hilogd异常重启，且hilog命令一直无法使用<br>2、hisysevent功能不可用，编译失败|
|14| 电源子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |

*特性质量评估可选项：特性不稳定，风险高\特性基本可用，遗留少量问题\特性质量良好*

### 4.1.2   新需求评价

*以表格的形式汇总新特性测试执行情况及遗留问题情况的评估,给出特性质量评估结论。*

| lssue号 | 特性名称 | 特性质量评估 | 约束依赖说明 | 备注 |
| ------- | -------- | ------------ | ------------ | ---- |
| I3ZVTJ  |【分布式任务调度子系统】接收远端拉起FA请求，跨设备拉起远端FA|特性基本可用|   |     |
| I3ZVTT  |【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-SAMGR模块构建 |特性基本可用|   |  |
| I3ZVT4  |【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-跨设备绑定元能力|特性基本可用|  |  |
| I49HB0  |【分布式任务调度子系统】启动、绑定能力添加组件visible权限校验| 特性基本可用 |   |      |
| [I3NT3F](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT3F) | 【轻内核子系统】内核支持trace功能 | 缺少使用指导书，开发者使用困难，风险高 |              |      |
| [I3NT63](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT63) | 【轻内核子系统】pagecache功能完善 | 特性基本可用，遗留少量问题 |              |      |
| [I3ZY55](https://gitee.com/open_harmony/dashboard?issue_id=I3ZY55) | 【Kernel升级适配】Hi3516DV300开发板适配OpenHarmony 5.10内核 | 特性质量良好 | | |
| I40MWK | 构建语言和地区配置和区域显示能力 | 特性基本可用 | | |
| I46W6Q | 【全球化】新增31种语言支持 | 特性基本可用 | | |
| [I3NT3F](https://gitee.com/openharmony/multimedia_media_standard/issues/I42TIB) |【媒体子系统】[Media][媒体录制器]音视频录制及API | 特性基本可用:音视频录制基本功能正常 |              | 音频录制有杂音，视频录制图库播放时画面显示不全|
| [I42TMC](https://gitee.com/openharmony/multimedia_camera_standard/issues/I42TMC) |【媒体子系统】[Camera][相机框架管理]相机录像功能 | 特性不稳定，风险高：基本录像功能可用但存在多个遗留问题 |              | 相机拍照录像存在拍照预览页面闪烁，录像后播放速度慢，播放没声音的问题，且用图库播放视频时画面显示不完全|
| I40OWT |【搜网】Radio状态管理 | 特性基本可用 | modem占用USB接口，导致USB HDC不可用。此为hi3516开发板约束。 | |
| I40OWU |【搜网】搜网注册 | 特性基本可用 | | |
| I40OU3 |【搜网】驻网信息(运营商信息及SPN广播GSM） | 特性基本可用 | | |
| I40OU5 |【搜网】网络状态(漫游、GSM/LTE/WCDMA接入技术） | 特性基本可用 | | |
| I40OU7 |【搜网】信号强度(WCDMA/GSM/LTE) | 特性基本可用 | | |
| I40OTY |【SIM卡】G/U卡文件信息获取、保存 | 特性基本可用 | | |
| I40OWN |【SIM卡】卡状态处理 | 特性基本可用 | | |
| I40OWO |【SIM卡】解锁卡pin、puk | 特性基本可用 | | |
| I40OWM |【SIM卡】卡账户处理 | 特性基本可用 | | |
| I40OSY |【SIM卡】提供拷贝、删除、更新、获取卡短信的能力供电话子系统内部使用 | 特性基本可用 | | |
| I40OUC |【短彩信】提供发送短信功能 | 特性基本可用 | | |
| I40OUF |【短彩信】提供接收短信功能 | 特性基本可用 | | |
| I40OX3 |【短彩信】提供小区广播的能力 | 特性基本可用 | | |
| [I48BOB](https://gitee.com/openharmony/distributeddatamgr_file/issues/I48BOB) |【分布式文件子系统】（需求）JS存储框架开发 | 特性不稳定，风险高 | | XTS自动化用例执行失败，相关JS接口调用失败 |
| [I4A3LY](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4A3LY) |【分布式文件子系统】（需求）JS存储框架挂载能力 - FS Manager | 特性不稳定，风险高 | | XTS自动化用例执行失败，相关JS接口调用失败 |
|         |     【DFX子系统】提供HiAppEvent JS API     |特性基本可用，无遗留问题              |              |      |
|         |     【DFX子系统】提供HiCollie卡死检测框架    |特性不可用，该特性代码未引入到LTS分支              |              |      |
|         |     【DFX子系统】提供HiTrace分布式调用链基础库     |特性不可用，该特性代码未引入到LTS分支              |              |      |
|  | |  | | |
|  | |  | | |
|  | |  | | |
|  | |  | | |
|  | |  | | |

用户程序框架&杂散&通知需求评估

| issue                                                        | 特性名称                                               | 特性质量评估 | 约束依赖说明                                                 | 备注 |
| ------------------------------------------------------------ | :----------------------------------------------------- | ------------ | ------------------------------------------------------------ | ---- |
| [I40PAV](https://gitee.com/open_harmony/dashboard?issue_id=I40PAV) | 发布开启一个有页面的Ability的WantAgent通知             | 特性基本可用 |                                                              |      |
| [I40PAW](https://gitee.com/open_harmony/dashboard?issue_id=I40PAW) | 通知删除接口                                           | 特性基本可用 |                                                              |      |
| [I40PAX](https://gitee.com/open_harmony/dashboard?issue_id=I40PAX) | 查看Active通知内容和Active通知个数的接口               | 特性基本可用 |                                                              |      |
| [I40PAY](https://gitee.com/open_harmony/dashboard?issue_id=I40PAY) | 通知取消订阅                                           | 特性基本可用 |                                                              |      |
| [I40PAZ](https://gitee.com/open_harmony/dashboard?issue_id=I40PAZ) | 通知订阅                                               | 特性基本可用 |                                                              |      |
| [I40PB6](https://gitee.com/open_harmony/dashboard?issue_id=I40PB6) | 应用侧增加slot                                         | 特性基本可用 |                                                              |      |
| [I40PB7](https://gitee.com/open_harmony/dashboard?issue_id=I40PB7) | 应用侧删除slot                                         | 特性基本可用 |                                                              |      |
| [I436VL](https://gitee.com/open_harmony/dashboard?issue_id=I436VL) | ces部件化改造                                          | 未交付       |                                                              |      |
| [I436VM](https://gitee.com/open_harmony/dashboard?issue_id=I436VM) | ans部件化改造                                          | 未交付       |                                                              |      |
| [I40PBC](https://gitee.com/open_harmony/dashboard?issue_id=I40PBC) | 应用侧发布本地分组的普通通知                           | 特性基本可用 |                                                              |      |
| [I40PBF](https://gitee.com/open_harmony/dashboard?issue_id=I40PBF) | 在免打扰模式下发布通知                                 | 特性基本可用 |                                                              |      |
| [I40PBG](https://gitee.com/open_harmony/dashboard?issue_id=I40PBG) | 发布开启一个无页面的Ability的wantAgent                 | 特性基本可用 |                                                              |      |
| [I40PBH](https://gitee.com/open_harmony/dashboard?issue_id=I40PBH) | 取消WantAgent的实例                                    | 特性基本可用 |                                                              |      |
| [I40PBI](https://gitee.com/open_harmony/dashboard?issue_id=I40PBI) | 发布公共事件的WantAgent通知                            | 特性基本可用 |                                                              |      |
| [I40PBM](https://gitee.com/open_harmony/dashboard?issue_id=I40PBM) | 应用侧取消本地通知                                     | 特性基本可用 |                                                              |      |
| [I40PBN](https://gitee.com/open_harmony/dashboard?issue_id=I40PBN) | 应用侧发布声音通知                                     | 特性基本可用 |                                                              |      |
| [I40PBO](https://gitee.com/open_harmony/dashboard?issue_id=I40PBO) | 应用侧发布振动通知                                     | 特性基本可用 |                                                              |      |
| [I40PBP](https://gitee.com/open_harmony/dashboard?issue_id=I40PBP) | 应用侧发布本地有输入框的通知（NotificationUserInput）  | 特性基本可用 |                                                              |      |
| [I40PBD](https://gitee.com/open_harmony/dashboard?issue_id=I40PBD) | 2个不同的slot，加入到同一个Slot组里面                  | 特性基本可用 |                                                              |      |
| [I40PBJ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBJ) | 提供管理角标显示/隐藏的接口                            | 特性基本可用 |                                                              |      |
| [I40PBK](https://gitee.com/open_harmony/dashboard?issue_id=I40PBK) | 提供管理通知许可的接口（设置和查询）                   | 特性基本可用 |                                                              |      |
| [I40PBL](https://gitee.com/open_harmony/dashboard?issue_id=I40PBL) | 应用删除SlotGroup                                      | 特性基本可用 |                                                              |      |
| [I40PBQ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBQ) | 发布带ActionButton的本地通知                           | 特性基本可用 |                                                              |      |
| [I40PBR](https://gitee.com/open_harmony/dashboard?issue_id=I40PBR) | 通知流控处理                                           | 特性基本可用 |                                                              |      |
| [I40PBT](https://gitee.com/open_harmony/dashboard?issue_id=I40PBT) | 死亡监听                                               | 特性基本可用 |                                                              |      |
| [I40PBU](https://gitee.com/open_harmony/dashboard?issue_id=I40PBU) | 通知shell命令                                          | 未交付       |                                                              |      |
| [I40PBB](https://gitee.com/open_harmony/dashboard?issue_id=I40PBB) | 应用侧发布本地图片类型通知                             | 特性不稳定   | 依赖多媒体的提供的图片处理能力                               |      |
| [I40PBS](https://gitee.com/open_harmony/dashboard?issue_id=I40PBS) | 通知图片大小限制                                       | 特性不稳定   | 依赖多媒体的提供的图片处理能力                               |      |
| [I4014F](https://gitee.com/open_harmony/dashboard?issue_id=I4014F) | 【帐号子系统】JS API交付，开源+小程序                  | 特性基本可用 |                                                              |      |
| [I43G6T](https://gitee.com/open_harmony/dashboard?issue_id=I43G6T) | WM Client架构演进                                      | 特性基本可用 |                                                              |      |
| [I43HMM](https://gitee.com/open_harmony/dashboard?issue_id=I43HMM) | WM Server架构演进                                      | 特性基本可用 |                                                              |      |
| [I40OPG](https://gitee.com/open_harmony/dashboard?issue_id=I40OPG) | 【定时服务】非功能性需求                               | 特性基本可用 |                                                              |      |
| [I40OPH](https://gitee.com/open_harmony/dashboard?issue_id=I40OPH) | 【定时服务】时间时区同步                               | 特性基本可用 |                                                              |      |
| [I40OPI](https://gitee.com/open_harmony/dashboard?issue_id=I40OPI) | 【定时服务】定时器功能                                 | 特性基本可用 |                                                              |      |
| [I40OPJ](https://gitee.com/open_harmony/dashboard?issue_id=I40OPJ) | 【定时服务】时间时区管理                               | 特性基本可用 |                                                              |      |
| [I436VX](https://gitee.com/open_harmony/dashboard?issue_id=I436VX) | 提供分布式的回调Native接口                             | 特性不稳定   | 本地ability迁出到远端设备（标准设备之间），只支持FA形式；<br />新增continueAbility接口；<br />存在问题：<br />1，迁出场景无法验收，代码合入在LTS后合入的；<br />2，ACE2.0 新增ets，需要在config.json中支持，代码合入在LTS后合入的；<br />2，传输KV长度JS与native规格不一致 ； |      |
| [I436N0](https://gitee.com/open_harmony/dashboard?issue_id=I436N0) | 支持应用包信息分布式存储能力                           | 特性不稳定   | 同上                                                         |      |
| [I436VH](https://gitee.com/open_harmony/dashboard?issue_id=I436VH) | 创建串行任务分发器，使用串行任务分发器执行任务         | 特性基本可用 |                                                              |      |
| [I436VI](https://gitee.com/open_harmony/dashboard?issue_id=I436VI) | 创建专有任务分发器，使用专有任务分发器执行任务         | 特性基本可用 |                                                              |      |
| [I436VJ](https://gitee.com/open_harmony/dashboard?issue_id=I436VJ) | 创建全局并发任务分发器，使用全局并发任务分发器执行任务 | 特性基本可用 |                                                              |      |
| [I436VK](https://gitee.com/open_harmony/dashboard?issue_id=I436VK) | 创建并发任务分发器，使用并发任务分发器执行任务         | 特性基本可用 |                                                              |      |
| [I4312I](https://gitee.com/open_harmony/dashboard?issue_id=I4312I) | 支持全新开发范式                                       | 未交付       |                                                              |      |
| [I436VT](https://gitee.com/open_harmony/dashboard?issue_id=I436VT) | 安装读取shortcut信息                                   | 未交付       |                                                              |      |
| [I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4) | JS应用生命周期补充支持                                 | 特性基本可用 |                                                              |      |
| [I43V4W](https://gitee.com/open_harmony/dashboard?issue_id=I43V4W) | 支持使用JS开发service ability                          | 特性基本可用 |                                                              |      |
| [I43V73](https://gitee.com/open_harmony/dashboard?issue_id=I43V73) | 支持使用JS开发data ability                             | 特性基本可用 |                                                              |      |
| [I43UZ0](https://gitee.com/open_harmony/dashboard?issue_id=I43UZ0) | 支持系统服务弹窗                                       | 未交付       |                                                              |      |
| [I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4) | JS应用生命周期补充支持                                 | 特性基本可用 |                                                              |      |
| [I43UU4](https://gitee.com/open_harmony/dashboard?issue_id=I43UU4) | JS应用生命周期补充支持                                 | 特性基本可用 |                                                              |      |
| [I43V4W](https://gitee.com/open_harmony/dashboard?issue_id=I43V4W) | 支持使用JS开发service ability                          | 特性不稳定   | 跨进程的connect\disconnect\sendrequest支持  <br /> 同进程的connect\disconnect\sendrequest不支持； |      |
| [I43V73](https://gitee.com/open_harmony/dashboard?issue_id=I43V73) | 支持使用JS开发data ability                             | 特性基本可用 | 只提供接口定义，使用方需要实现具体业务逻辑；当前只支持JS应用导致Native侧，不支持JS应用到JS服务（ACE交付范围，不在用户程序框架） |      |

## 4.2   兼容性测试结论

*补充兼容性测试报告*

## 4.3   安全专项测试结论

*要求：无严重的隐私安全问题遗留，版本通过漏洞扫描，未解决的漏洞满足以下要求：*

*a）无严重及以上公开漏洞；*

*b）无已公开60天但未修复的一般（CVSS得分4及以上）安全漏洞；*

*c）不存在已知CVE安全漏洞，业界未解决的已知CVE安全漏洞可例外；*

*d）无法修复，且经过社区开发团队综合评估备案允许遗留的漏洞；*

## 4.4   稳定性专项测试结论

*a）执行反复开关机压力测试，运行5000次，无不开机问题；*

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|     开关机测试     | 整机稳定性           | 无不开机、无整机稳定性问题         | 是          |测试结果：多个设备重启之后，hdc无法自动发现重启后的设备，需要重新kill掉hdc才能发现设备                             |   https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT?from=project-issue        |
|     xts压测     | 各子系统特性在整机测试下的稳定性           | 无稳定性问题         |是          | 测试结果：无异常<br>风险：xtx全量用例执行一轮需要消耗时间酒，当前全量用例测试轮次数少</br>                            |           |

## 4.5   性能专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：1、静态KPI通过率100%，2、开关机及动态内存整机达标，子系统无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|          |            |          |          |                             |           |
|          |            |          |          |                             |           |

## 4.6   功耗专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|          |            |          |          |                             |           |
|          |            |          |          |                             |           |

# 5   问题单统计

[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试I3NT3F内核支持trace功能缺少指导书无法测试，开发者使用难度较大 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BN00)

[【openHarmony】【3.0.0.8】【轻内核子系统】集成测试 在执行ActsTimeiTest.bin脚本，发现打印的错误信息单词拼错。](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BLE8)

[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试fs_posix模块nfs用例跑多次会出现不停打印申请内存失败问题](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BL3S)

[【OpenHarmony】【3.0.0.8】【轻内核子系统】dyload_posix模块在removefile的时候出现错误 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BJFU)

[【OpenHarmony】【3.0.0.6】【轻内核子系统】FutexTest.testPthreadTimdOutRWlockWR用例执行失败](https://gitee.com/openharmony/kernel_liteos_a/issues/I490KZ)

[【WIP】【OpenHarmony】【3.0.0.2】【轻内核子系统】集成测试toybox需提供用户手册，手册中需要将各命令的限制详细说明，以便更好的指导... ](https://gitee.com/openharmony/third_party_toybox/issues/I44YLY)

[【OpenHarmony】【20210701】【轻内核子系统】xts权限用例压测用户态概率异常 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ZJ1D)

[【openHarmony】【3.0 Beta1】【轻内核子系统】集成测试 在执行ActsIpcShmTest.bin脚本，出现大量未释放的共享内存。](https://gitee.com/openharmony/kernel_liteos_a/issues/I47X2Z)

[【OpenHarmony】【1.1.577】【轻内核子系统】集成测试在某个目录下mv一个文件后，再在此目录下创建同名文件并二次mv该文件失败，提示此文件不存在](https://gitee.com/openharmony/third_party_toybox/issues/I44SFO)

[【OpenHarmony】【1.1.541】【轻内核子系统】集成测试toybox 命令kill、cp、mv等命令有些操作提示信息不合理，有些操作回显中有e..](https://gitee.com/openharmony/third_party_toybox/issues/I42VH1)

[【OpenHarmony】【20210726】【轻内核子系统】集成测试直接执行cat后无法退出，需要重启设备恢复](https://gitee.com/openharmony/third_party_mksh/issues/I42N33)

[【OpenHarmony】【3.0.0.9】【媒体子系统】视频录制后播放没声音](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXY1)

[【OpenHarmony】【3.0.0.9】【媒体子系统】相机预览时候画面出现闪烁](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXYV)

[【OpenHarmony】【3.0.0.9】【媒体子系统】音频录制后播放有杂音](https://gitee.com/openharmony/multimedia_media_standard/issues/I4BXWY)

[【OpenHarmony】【3.0.0.9】【媒体子系统】相机录制视频无声音，在图库播放时画面显示不全](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXDQ)

[【OpenHarmony】【3.0.0.9】【媒体子系统】camera_sample和player_sample测试用例失败](https://gitee.com/openharmony/applications_sample_camera/issues/I4BYF9?from=project-issue)

[【OpenHarmony】【3.0.0.9】【媒体子系统】audio_sample测试用例失败](https://gitee.com/openharmony/applications_sample_camera/issues/I4BYEC?from=project-issue)

[【OpenHarmony】【3.0.0.9】【媒体子系统】3516liteo与linux 手动测试失败，查看照片和录像视频页面会卡，打不开](https://gitee.com/openharmony/applications_sample_camera/issues/I4BYFO?from=project-issue)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板camera驱动压测问题](https://gitee.com/openharmony/drivers_framework/issues/I4BWKC)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板ResetDriver接口失败](https://gitee.com/openharmony/drivers_framework/issues/I4BW0G)

[【OpenHarmony 3.0.0.6】【驱动子系统】L1 3516DV300单板liteos版本调用AllocMem接口测试，单板挂死](https://gitee.com/openharmony/drivers_peripheral/issues/I48A2I)

[【OpenHarmony 3.0 Beta1】【驱动子系统】不安全函数和安全编译选项](https://gitee.com/openharmony/drivers_peripheral/issues/I47DE6)





